import datetime
import functools
from collections import namedtuple

import requests
from requests.auth import HTTPBasicAuth


class PostError(Exception):
    pass


class YearlyReps:

    URL = 'https://yearlyreps.tompaton.com/'
    Goal = namedtuple('Goal', 'goal_id goal target notes')
    Rep = namedtuple('Rep', 'goal_id rep_id date count notes')

    def __init__(self, uid, pwd):
        self._uid = uid
        self._pwd = pwd

    def get_goals(self):
        url = self.URL + 'goals'
        return [self.Goal(**row)
                for row in self._send_request('GET', url)]

    def get_reps(self, goal_id):
        url = self.URL + f'goals/{goal_id}/reps'
        return [self._rep(row)
                for row in self._send_request('GET', url)]

    def _rep(self, row):
        row['date'] = datetime.date.fromisoformat(row['date'])
        return self.Rep(**row)

    def add_rep(self, goal_id, date=None, count=0, notes=None):
        url = self.URL + f'goals/{goal_id}/reps'
        if date is None:
            date = datetime.date.today()
        data = {'date': date.isoformat(), 'count': count}
        if notes is not None:
            data['notes'] = notes
        return self._rep(self._send_request('POST', url, json=data))

    def delete_rep(self, goal_id, rep_id):
        url = self.URL + f'goals/{goal_id}/reps/{rep_id}'
        return self._send_request('DELETE', url)

    def get_status(self, goal_id):
        url = self.URL + f'goals/{goal_id}/status-v2'
        return self._send_request('GET', url)

    def _send_request(self, method, url, **kwargs):  # pragma: no cover
        req = requests.Request(method, url,
                               auth=HTTPBasicAuth(self._uid, self._pwd),
                               headers={'Accept': 'application/json'},
                               **kwargs)
        res = requests.Session().send(req.prepare())
        if res.status_code == 200:
            return res.json()
        else:
            raise PostError(res.text)


class Builder():
    def __init__(self):
        self._output = ''
        self.ok = True

    def output(self, output):
        if self._output:
            if not (self._output.endswith(' ') or self._output.endswith('\n')):
                self._output += ' '
        self._output += output

    def error(self, error):
        self.ok = False
        if self._output:
            if not self._output.endswith('\n'):
                self._output += '\n'
        self._output += f"ERROR: {error}\n"

    def done(self):
        return self._output

    @staticmethod
    def chainable(method):
        @functools.wraps(method)
        def wrapper(self, *args, **kwargs):
            if self.ok:
                try:
                    method(self, *args, **kwargs)
                except PostError as e:
                    self.error(str(e))
            return self
        return wrapper


class YearlyReps2(Builder):
    def __init__(self, uid, pwd):
        super().__init__()
        self.reps = YearlyReps(uid, pwd)
        self._goal = None

    @property
    def status_url(self):
        return self.reps.URL + 'goals/status'

    @Builder.chainable
    def goal(self, goal_name):
        for goal in self.reps.get_goals():
            # TODO: dwim matching
            if goal.goal.lower() == goal_name.lower():
                self._goal = goal
                break

        if self._goal is None:
            self.error(f'Goal "{goal_name}" not found')

    @Builder.chainable
    def add_rep(self, date=None, count=0, notes=None):
        rep = self.reps.add_rep(self._goal.goal_id, date=date, count=count,
                                notes=notes)
        self.output(f'{rep.date.isoformat()}: {rep.count}')

    @Builder.chainable
    def status(self):
        periods = self.reps.get_status(self._goal.goal_id)
        status = ' '.join(f'{name[0]}{days:+d}'
                          for name, days, value, target
                          in periods)
        name, days, value, target = periods[0]
        self.output(f'{self._goal.goal} {value}/{target} {status}')
