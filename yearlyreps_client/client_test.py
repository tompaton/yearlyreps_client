import datetime

import pytest
from unittest.mock import patch, call
from freezegun import freeze_time

import yearlyreps_client as client


@pytest.fixture
def reps():
    return client.YearlyReps('uid', 'pwd')


@pytest.fixture
def goals_json():
    return [{"goal_id": 1,
             "goal": "Pullups",
             "target": 4200,
             "notes": "12 per day, 2 weeks off"},
            {"goal_id": 2,
             "goal": "Pushups",
             "target": 8750,
             "notes": "25 per day, 2 weeks off"}]


@pytest.fixture
def goal1():
    return client.YearlyReps.Goal(
            1, 'Pullups', 4200, '12 per day, 2 weeks off')


@pytest.fixture
def goals_tuples(goal1):
    return [
        goal1,
        client.YearlyReps.Goal(
            2, 'Pushups', 8750, '25 per day, 2 weeks off'),
    ]


@pytest.fixture
def goal1_status():
    return [['Year', -10, 900, 1000],
            ['Quarter', -2, 240, 250],
            ['Month', 0, 100, 100],
            ['Week', 1, 11, 10]]


@pytest.fixture
def reps_json():
    return [{"goal_id": 1,
             "rep_id": 35,
             "date": "2019-03-01",
             "count": 12,
             "notes": None},
            {"goal_id": 1,
             "rep_id": 39,
             "date": "2019-03-02",
             "count": 20,
             "notes": "12 + 8"}]


@pytest.fixture
def new_rep():
    return client.YearlyReps.Rep(1, 40, datetime.date(2019, 3, 3),
                                 10, None)


@pytest.fixture
def new_rep_json():
    return {
        'goal_id': 1, 'rep_id': 40, 'date': '2019-03-03', 'count': 10,
        'notes': None
    }


@pytest.fixture
def new_rep_note():
    return client.YearlyReps.Rep(1, 40, datetime.date(2019, 3, 3),
                                 10, 'note')


@pytest.fixture
def new_rep_note_json():
    return {
        'goal_id': 1, 'rep_id': 40, 'date': '2019-03-03', 'count': 10,
        'notes': 'note'
    }


@pytest.fixture
def reps_tuples():
    return [
        client.YearlyReps.Rep(1, 35, datetime.date(2019, 3, 1), 12, None),
        client.YearlyReps.Rep(1, 39, datetime.date(2019, 3, 2), 20,
                              '12 + 8'),
    ]


def test_get_goals(reps, goals_json, goals_tuples):
    with patch.object(reps, '_send_request', return_value=goals_json) as send:
        assert reps.get_goals() == goals_tuples

        send.assert_called_with(
            'GET', 'https://yearlyreps.tompaton.com/goals')


def test_get_reps(reps, reps_json, reps_tuples):
    with patch.object(reps, '_send_request', return_value=reps_json) as send:
        assert reps.get_reps(1) == reps_tuples

        send.assert_called_with(
            'GET', 'https://yearlyreps.tompaton.com/goals/1/reps')


def test_add_rep(reps, new_rep, new_rep_json):
    with patch.object(reps, '_send_request',
                      return_value=new_rep_json) as send:
        assert reps.add_rep(1, date=datetime.date(2019, 3, 3), count=10) \
            == new_rep

        assert send.mock_calls == [
            call('POST', 'https://yearlyreps.tompaton.com/goals/1/reps',
                 json={'date': '2019-03-03', 'count': 10}),
        ]


@freeze_time('2019-03-04')
def test_add_rep2(reps, new_rep_note, new_rep_note_json):
    with patch.object(reps, '_send_request',
                      return_value=new_rep_note_json) as send:
        assert reps.add_rep(1, count=10, notes='note') \
            == new_rep_note

        assert send.mock_calls == [
            call('POST', 'https://yearlyreps.tompaton.com/goals/1/reps',
                 json={'date': '2019-03-04', 'count': 10, 'notes': 'note'}),
        ]


def test_add_rep_error(reps):
    with patch.object(reps, '_send_request',
                      side_effect=client.PostError('error')) as send:
        with pytest.raises(client.PostError) as execinfo:
            reps.add_rep(1, date=datetime.date(2019, 3, 3))

        assert str(execinfo.value) == 'error'

        assert send.mock_calls == [
            call('POST', 'https://yearlyreps.tompaton.com/goals/1/reps',
                 json={'date': '2019-03-03', 'count': 0}),
        ]


def test_delete_rep(reps):
    with patch.object(reps, '_send_request', return_value=True) as send:
        assert reps.delete_rep(1, 40)

        assert send.mock_calls == [
            call('DELETE',
                 'https://yearlyreps.tompaton.com/goals/1/reps/40'),
        ]


def test_delete_rep_error(reps):
    with patch.object(reps, '_send_request',
                      side_effect=client.PostError('error')) as send:
        with pytest.raises(client.PostError) as execinfo:
            reps.delete_rep(1, 40)

        assert str(execinfo.value) == 'error'

        assert send.mock_calls == [
            call('DELETE',
                 'https://yearlyreps.tompaton.com/goals/1/reps/40'),
        ]


def test_get_status(reps, goal1_status):
    with patch.object(reps, '_send_request',
                      return_value=goal1_status) as send:
        assert reps.get_status(1) == goal1_status

        send.assert_called_with(
            'GET', 'https://yearlyreps.tompaton.com/goals/1/status-v2')


@pytest.fixture
def reps2():
    return client.YearlyReps2('uid', 'pwd')


def test_builder_goal(reps2, goals_tuples):
    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.get_goals.return_value = goals_tuples

        assert reps2.goal('Pullups') == reps2
        assert reps2._goal.goal_id == 1

        assert mock_reps.mock_calls == [call.get_goals()]


def test_builder_match(reps2, goals_tuples):
    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.get_goals.return_value = goals_tuples

        assert reps2.goal('pullups') == reps2
        assert reps2._goal.goal_id == 1

        assert mock_reps.mock_calls == [call.get_goals()]


def test_builder_goal_not_found(reps2, goals_tuples):
    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.get_goals.return_value = goals_tuples

        assert reps2.goal('Unknown') == reps2
        assert reps2._goal is None
        assert reps2._output == 'ERROR: Goal "Unknown" not found\n'

        assert mock_reps.mock_calls == [call.get_goals()]


def test_builder_goal_error(reps2):
    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.get_goals.side_effect = client.PostError('error')

        assert reps2.goal('Unknown') == reps2
        assert reps2._goal is None
        assert reps2._output == 'ERROR: error\n'

        assert mock_reps.mock_calls == [call.get_goals()]


def test_builder_add_rep(reps2, goal1, new_rep):
    date = datetime.date(2019, 3, 3)
    reps2._goal = goal1

    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.add_rep.return_value = new_rep
        assert reps2.add_rep(date=date, count=10) == reps2
        assert reps2._output == '2019-03-03: 10'
        assert mock_reps.mock_calls == [
            call.add_rep(1, count=10, date=date, notes=None)
        ]


def test_builder_add_rep_error_before(reps2):
    date = datetime.date(2019, 3, 3)
    reps2.ok = False

    with patch.object(reps2, 'reps') as mock_reps:
        assert reps2.add_rep(date=date, count=10) == reps2

        assert mock_reps.mock_calls == []


def test_builder_add_rep_error(reps2, goal1):
    date = datetime.date(2019, 3, 3)
    reps2._goal = goal1

    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.add_rep.side_effect = client.PostError('error')

        assert reps2.add_rep(date=date, count=10) == reps2
        assert reps2._output == 'ERROR: error\n'

        assert mock_reps.mock_calls == [
            call.add_rep(1, count=10, date=date, notes=None)
        ]


def test_builder_status(reps2, goal1, goal1_status):
    reps2._goal = goal1

    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.get_status.return_value = goal1_status
        assert reps2.status() == reps2
        assert reps2._output == 'Pullups 900/1000 Y-10 Q-2 M+0 W+1'

        assert mock_reps.mock_calls == [
            call.get_status(1),
        ]


def test_builder_status_error_before(reps2):
    reps2.ok = False
    with patch.object(reps2, 'reps') as mock_reps:
        assert reps2.status() == reps2

        assert mock_reps.mock_calls == []


def test_builder_add_rep_chain(reps2, goals_tuples, new_rep, goal1_status):
    date = datetime.date(2019, 3, 3)

    with patch.object(reps2, 'reps') as mock_reps:
        mock_reps.get_goals.return_value = goals_tuples
        mock_reps.add_rep.return_value = new_rep
        mock_reps.get_status.return_value = goal1_status
        assert (reps2.goal('Pullups')
                .add_rep(date=date, count=10)
                .status()
                .done()
                == '2019-03-03: 10 Pullups 900/1000 Y-10 Q-2 M+0 W+1')
        assert mock_reps.mock_calls == [
            call.get_goals(),
            call.add_rep(1, count=10, date=date, notes=None),
            call.get_status(1),
        ]


def test_builder_status_url(reps2):
    assert reps2.status_url == 'https://yearlyreps.tompaton.com/goals/status'
